﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_windowsCalcLogin
{
    public partial class Cryptology : Form
    {
        Crypt crypt = new Crypt();
        public Cryptology()
        {
            InitializeComponent();
        }

        private void txtString_TextChanged(object sender, EventArgs e)
        {
            var cntls = GetAll(this, typeof(RadioButton));
            foreach (Control cntrl in cntls)
            {
                RadioButton radioButton = (RadioButton)cntrl;
                if (radioButton == btnCeaserE)
                {
                    btnCeaserE_CheckedChanged(sender, e);
                }
                else if (radioButton == btnCeaserD)
                {
                    btnCeaserD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreD)
                {
                    btnVigenèreD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreE)
                {
                    btnVigenèreE_CheckedChanged(sender, e);
                }
            }
        }

        private void PctBoxArrow_Click(object sender, EventArgs e)
        {
            Selection S = new Selection();
            S.Show();
            this.Hide();
        }

        private void btnCeaserE_CheckedChanged(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(txtAlphabet.Text) && !String.IsNullOrEmpty(txtString.Text) && !String.IsNullOrEmpty(txtKey.Text))
            {
                if (btnCeaserE.Checked)
                {
                    crypt.TextConvert(txtAlphabet.Text.ToString());
                    crypt.SetCeaserKey(txtKey.Text);
                    lblResult.Text = crypt.CeasarCipher(txtString.Text);
                }
            }                    
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();
            return controls.SelectMany(ctrls => GetAll(ctrls, type)).Concat(controls).Where(c => c.GetType() == type);
        }
        private void txtAlphabet_TextChanged(object sender, EventArgs e)
        {

            var cntls = GetAll(this, typeof(RadioButton));
            foreach (Control cntrl in cntls)
            {
                RadioButton radioButton = (RadioButton)cntrl;
                if (radioButton==btnCeaserE)
                {
                    btnCeaserE_CheckedChanged(sender,e);
                }
                else if (radioButton == btnCeaserD)
                {
                    btnCeaserD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreD)
                {
                    btnVigenèreD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreE)
                {
                    btnVigenèreE_CheckedChanged(sender, e);
                }
            }
        }
        private void Cryptology_Load(object sender, EventArgs e)
        {
            var cntls = GetAll(this, typeof(RadioButton));
            foreach (Control cntrl in cntls)
            {
                RadioButton radioButton = (RadioButton)cntrl;
                if (radioButton.Checked)
                {
                    radioButton.Checked = false;
                }
            }
        }

        private void btnCeaserD_CheckedChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAlphabet.Text) && !String.IsNullOrEmpty(txtString.Text) && !String.IsNullOrEmpty(txtKey.Text))
            {
                if (btnCeaserD.Checked)
                {
                    crypt.TextConvert(txtAlphabet.Text.ToString());
                    crypt.SetCeaserKey(txtKey.Text);
                    lblResult.Text = crypt.CeaserCipherDecrypt(txtString.Text);
                }
            }
        }

        private void btnVigenèreE_CheckedChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAlphabet.Text) && !String.IsNullOrEmpty(txtString.Text) && !String.IsNullOrEmpty(txtKey.Text))
            {
                if (btnVigenèreE.Checked)
                {
                    crypt.TextConvert(txtAlphabet.Text.ToString());
                    crypt.SetVigenereKey(txtKey.Text);
                    lblResult.Text = crypt.VigenèreCipher(txtString.Text);
                }
            }
        }

        private void btnVigenèreD_CheckedChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAlphabet.Text) && !String.IsNullOrEmpty(txtString.Text) && !String.IsNullOrEmpty(txtKey.Text))
            {
                if (btnVigenèreD.Checked)
                {
                    crypt.TextConvert(txtAlphabet.Text.ToString());
                    crypt.SetVigenereKey(txtKey.Text);
                    lblResult.Text = crypt.VigenèreCipherDecrypt(txtString.Text);
                }
            }
        }

        private void txtKey_TextChanged(object sender, EventArgs e)
        {
            var cntls = GetAll(this, typeof(RadioButton));
            foreach (Control cntrl in cntls)
            {
                RadioButton radioButton = (RadioButton)cntrl;
                if (radioButton == btnCeaserE)
                {
                    btnCeaserE_CheckedChanged(sender, e);
                }
                else if (radioButton == btnCeaserD)
                {
                    btnCeaserD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreD)
                {
                    btnVigenèreD_CheckedChanged(sender, e);
                }
                else if (radioButton == btnVigenèreE)
                {
                    btnVigenèreE_CheckedChanged(sender, e);
                }
            }
        }

        private void lblHeading_Click(object sender, EventArgs e)
        {

        }
    }
}
