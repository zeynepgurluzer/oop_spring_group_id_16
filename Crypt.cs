﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_windowsCalcLogin
{

    public class Crypt
    {
        int CeaserKey = 0;
        string vigenereKey;
        StringBuilder longKey;
        StringBuilder Alphabet;
        StringBuilder reverseAlphabet;
        public Crypt()
        {
            Alphabet = new StringBuilder();
            reverseAlphabet = new StringBuilder();
            longKey = new StringBuilder();
        }
        public void SetCeaserKey(string str)
        {
            int.TryParse(str, out CeaserKey);
        }
        public void SetVigenereKey(string str)
        {
            vigenereKey = str;
        }
        public void SetvigenereKey(string str)
        {
            vigenereKey = str;
        }
        public void TextConvert(string str)
        {
            Alphabet.Clear();
            reverseAlphabet.Clear();
            for (int i = 0; i < str.Length; i++)
            {
                Alphabet.Append(str[i]);
            }
            for (int i = Alphabet.Length-1; i >= 0; i--)
            {
                reverseAlphabet.Append(Alphabet[i]);
            }
        }
        public string CeasarCipher(string str)
        {
            StringBuilder Result = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (str[i] == Alphabet[j])
                    {
                        int mod = CeaserKey % Alphabet.Length;
                        if (mod + j > Alphabet.Length - 1)
                        {
                            mod = (mod + j) % Alphabet.Length;
                            Result.Append(Alphabet[mod]);
                        }
                        else
                        {
                            Result.Append(Alphabet[mod + j]);
                        }
                        break;

                    }
                }
            }
            return Result.ToString();
        }
        public string CeaserCipherDecrypt(string str)
        {
            StringBuilder Result = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j < reverseAlphabet.Length; j++)
                {
                    if (str[i] == reverseAlphabet[j])
                    {
                        int mod = CeaserKey % reverseAlphabet.Length;
                        if (mod + j > reverseAlphabet.Length - 1)
                        {
                            mod = (mod + j) % reverseAlphabet.Length;
                            Result.Append(reverseAlphabet[mod]);
                        }
                        else
                        {
                            Result.Append(reverseAlphabet[mod + j]);
                        }
                        break;
                       
                    }

                }
            }
            return Result.ToString();
        }

        private void getLongKey(int length)
        {
            StringBuilder key = new StringBuilder();

           int control = 0;
            for (int i = 0; i < length; i++)
            {
                if (control < vigenereKey.Length)
                {
                    key.Append(vigenereKey[control]);
                    control++;
                }
                else
                {
                    control = 0;
                    key.Append(vigenereKey[control]);
                    control++;
                }
            }
            longKey = key;
        }
        public string VigenèreCipher(string str)
        {
            StringBuilder Result = new StringBuilder();
            getLongKey(str.Length);
            int[] stringIndex, keyIndex;
            stringIndex = new int[str.Length];
            keyIndex = new int[str.Length];
            int stringControl = 0, keyControl = 0;
            for (int i = 0; i < str.Length; i++) //string
            {
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (str[i] == Alphabet[j])
                    {
                        stringIndex[stringControl] = j;
                        stringControl++;
                    }
                    if (longKey[i] == Alphabet[j])
                    {
                        keyIndex[keyControl] = j;
                        keyControl++;
                    }
                }
            }
            for (int i = 0; i < str.Length; i++)
            {
                int mod = (stringIndex[i] + keyIndex[i]) % Alphabet.Length;
                mod++;
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (j == mod)
                    {
                        Result.Append(Alphabet[j]);
                    }
                }
            }
            return Result.ToString();
        }
        public string VigenèreCipherDecrypt(string str)
        {
            StringBuilder Result = new StringBuilder();
            getLongKey(str.Length);

            int[] stringIndex, keyIndex;
            stringIndex = new int[str.Length];
            keyIndex = new int[str.Length];
            int stringControl = 0, keyControl = 0;
            for (int i = 0; i < str.Length; i++) //string
            {
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (str[i] == Alphabet[j])
                    {
                        stringIndex[stringControl] = j;
                        stringControl++;
                    }
                    if (longKey[i] == Alphabet[j])
                    {
                        keyIndex[keyControl] = j;
                        keyControl++;
                    }
                }
            }
            for (int i = 0; i < str.Length; i++)
            {
                int mod = ((stringIndex[i] - keyIndex[i])+Alphabet.Length)% Alphabet.Length;
                mod--;
                if (mod < 0)
                {
                    mod += Alphabet.Length;
                }
                
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (j == mod)
                    {
                        Result.Append(Alphabet[j]);
                    }
                }
            }
            return Result.ToString();
        }

    }
}