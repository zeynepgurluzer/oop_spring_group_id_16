﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_windowsCalcLogin
{
   
    public partial class sign_up : Form
    {
        private loginForm mainForm = null;
        Hash hash1 = new Hash();
        Hash hash2 = new Hash();
        User user = new User();
        public sign_up(Form callingForm)
        {
            mainForm = callingForm as loginForm;
            InitializeComponent();
        }
 
        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void username_textbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_sign_up_Click(object sender, EventArgs e)
        {
            string username = "";
            username = username_textbox.Text;

            string password = "";
            password = password_textbox.Text;

            string password2 = "";
            password2 = password2_textbox.Text;

            if (!String.IsNullOrEmpty(username_textbox.Text) && !String.IsNullOrEmpty(password_textbox.Text) && !String.IsNullOrEmpty(password2_textbox.Text))
            {

                hash1.Encrypt(password);
                hash2.Encrypt(password2);
                if (hash1.getPass() == hash2.getPass())
                {
                    user.setUsername(username);
                    user.setHash(hash1);
                    user.setToArray();
                    this.Close();
                }

                else
                {
                    MessageBox.Show("The Password cannot confirmed!");
                }
            }
        }

        private void password_textbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void goster1_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (goster1_checkbox.Checked)
            { 
                password_textbox.PasswordChar = '\0';
                goster1_checkbox.Text = "Hide Password";
            } 
            
            else 
            { 
                password_textbox.PasswordChar = '*';
                goster1_checkbox.Text = "Show Password";
            }
        }

        private void sign_up_Load(object sender, EventArgs e)
        {
            password_textbox.PasswordChar = '*';
            password2_textbox.PasswordChar = '*';
            this.mainForm.u = user;
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (goster2_checkbox.Checked)
            {
                password2_textbox.PasswordChar = '\0';
                goster2_checkbox.Text = "Hide Password";
            }

            else
            {
                password2_textbox.PasswordChar = '*';
                goster2_checkbox.Text = "Show Password";
            }
        }

        private void label_password_Click(object sender, EventArgs e)
        {

        }
    }
}
