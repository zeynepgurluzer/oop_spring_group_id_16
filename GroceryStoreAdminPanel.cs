﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Media.Imaging;
namespace oop_windowsCalcLogin
{
    public partial class GroceryStoreAdminPanel : Form
    {
        public GroceryStoreAdminPanel()
        {
            InitializeComponent();
        }
        
        static int numcount = 10;
        // Create an array to hold all your objects
        GroceryItems[] ProductObjects = new GroceryItems[20];
 
        private void CreateTenProduct()
        {
            string fileName; string path;
            Image image;

            ProductObjects[0] = new GroceryItems("Pineapple", 2, "Fruit Type");
            fileName = "pineapple.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[0].PictureBox = image;

            ProductObjects[1] = new GroceryItems("Strawberry", 1.20, "Fruit Type");
            fileName = "strawberry.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[1].PictureBox = image;

            ProductObjects[2] = new GroceryItems("Eggplant", 0.50, "Vegetable Type");
            fileName = "eggplant.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[2].PictureBox = image;

            ProductObjects[3] = new GroceryItems("Peach", 0.75, "Fruit Type");
            fileName = "peach.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[3].PictureBox = image;

            ProductObjects[4] = new GroceryItems("Tomato", 0.30, "Vegetable Type");
            fileName = "tomato.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[4].PictureBox = image;

            ProductObjects[5] = new GroceryItems("Cherry", 0.90, "Fruit Type");
            fileName = "cherry.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[5].PictureBox = image;

            ProductObjects[6] = new GroceryItems("Parsley", 0.20, "Vegetable Type");
            fileName = "parsley.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[6].PictureBox = image;

            ProductObjects[7] = new GroceryItems("Watermelon", 1.50, "Fruit Type");
            fileName = "watermelon.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[7].PictureBox = image;

            ProductObjects[8] = new GroceryItems("Spinach", 0.80, "Vegetable Type");
            fileName = "spinach.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[8].PictureBox = image;

            ProductObjects[9] = new GroceryItems("Broccoli", 1.89, "Vegetable Type");
            fileName = "broccoli.png";
            path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            image = Image.FromFile(path);
            ProductObjects[9].PictureBox = image;
        }

        private void GroceryStoreAdminPanel_Load(object sender, EventArgs e)
        {
            CreateTenProduct();

            for (int i = 0; i < 10; i++) 
            {
                string[] row ={ProductObjects[i].Name,ProductObjects[i].Price.ToString(),ProductObjects[i].Description};

                ListViewItem mySatir = new ListViewItem(row);
                ListView1.Items.Add(mySatir);
            }

        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }
        public string GetMatchingImages(string path, string keyword)
        {
            var images = System.IO.Directory.GetFiles(path);

            foreach (var image in images)
            {
                if (image.Contains(keyword))
                {
                    return image;
                }
            }

            return "";
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtName.Text) && !String.IsNullOrEmpty(txtPrice.Text) && !String.IsNullOrEmpty(txtDescription.Text))
            {
                GroceryItems ProductAdd = new GroceryItems(txtName.Text, double.Parse(txtPrice.Text), txtDescription.Text);
                ProductObjects[numcount] = ProductAdd;

                string[] row = { ProductObjects[numcount].Name, ProductObjects[numcount].Price.ToString(), ProductObjects[numcount].Description };

                ListViewItem mySatir = new ListViewItem(row);
                ListView1.Items.Add(mySatir);

                txtName.Text = "";
                txtPrice.Text = "";
                txtDescription.Text = "";

                OpenFileDialog open = new OpenFileDialog();
                open.Filter = "Image Files(*.jpeg; *.jpg; *.png;)|*.jpeg; *.jpg; *.png;";

                if (open.ShowDialog() == DialogResult.OK)
                {
                    pictureBoxImage.Image = new Bitmap(open.FileName);

                    ProductObjects[numcount].PictureBox = pictureBoxImage.Image;
                }

                numcount++;
            }
          
        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListView1.FocusedItem.Index != -1)
                pictureBoxImage.Image = new Bitmap(ProductObjects[ListView1.FocusedItem.Index].PictureBox);
        }

        private void Update_Click(object sender, EventArgs e)
        {
            double result;
            if (!String.IsNullOrEmpty(txtName.Text))
            {
                ProductObjects[ListView1.FocusedItem.Index].Name = txtName.Text;
                ListView1.Items[ListView1.FocusedItem.Index].SubItems[0].Text = txtName.Text;
            }

            if(!String.IsNullOrEmpty(txtPrice.Text))
            {
                if(double.TryParse(txtPrice.Text, out result))
                {
                    ProductObjects[ListView1.FocusedItem.Index].Price = result;
                    ListView1.Items[ListView1.FocusedItem.Index].SubItems[1].Text = txtPrice.Text;
                }                            
            }

            if(!String.IsNullOrEmpty(txtDescription.Text))
            {
                ProductObjects[ListView1.FocusedItem.Index].Description = txtDescription.Text;
                ListView1.Items[ListView1.FocusedItem.Index].SubItems[2].Text = txtDescription.Text;
            }
            
        }

        private void btnUpdateImg_Click(object sender, EventArgs e)
        {
                OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpeg; *.jpg; *.png;)|*.jpeg; *.jpg; *.png;";

            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(open.FileName);

                ProductObjects[ListView1.FocusedItem.Index].PictureBox = pictureBoxImage.Image;
            }
        }
        
        private void Delete_Click(object sender, EventArgs e)
        {
            int index = ListView1.FocusedItem.Index;
            ListView1.Items.RemoveAt(index);

            for (int i = index; i < 19; i++)
            {
                ProductObjects[i] = ProductObjects[i + 1];
            }
            numcount--;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Selection slc = new Selection();
            slc.Show();
            this.Hide();
        }
    }
}
