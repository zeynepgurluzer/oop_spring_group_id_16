﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using oop_windowsCalcLogin;
namespace oop_windowsCalcLogin
{
    public struct UserInfo
    {
       public string username;
       public Hash password;
    }
    public class Hash
    {
        private string password;

        public string getPass()
        {
            return password;
        }
        public void Encrypt(string text)
        {

            MD5 md5 = new MD5CryptoServiceProvider();

            //metnin boyutuna göre hash hesaplar
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //hesapladıktan sonra hashi alır
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //her baytı 2 hexadecimal hane olarak değiştirir
                strBuilder.Append(result[i].ToString("x2"));
            }

            password = strBuilder.ToString();
        }
    }
    public class User
    {
        UserInfo user;
        static int count = 0;
        private static UserInfo[] users = new UserInfo[50];
        private Hash h;

        public User()
        {
            user = new UserInfo();
        }
        public void setToArray()
        {
            users[count] = user;
            count++;
        }
        public void setUsername(string username)
        {
            user.username = username;
        }
        public void setHash(Hash hash)
        {
            user.password = hash;
        }
        public string getUsername()
        {
            return user.username;
        }
        public bool isValid(string btnusername, string btnpassword)
        {
            foreach (var item in users)
            {
                h = new Hash();
                h.Encrypt(btnpassword);
                if (h.getPass() == item.password.getPass() && btnusername == item.username)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
