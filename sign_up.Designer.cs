﻿
namespace oop_windowsCalcLogin
{
    partial class sign_up
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sign_up));
            this.username_textbox = new System.Windows.Forms.TextBox();
            this.password_textbox = new System.Windows.Forms.TextBox();
            this.password2_textbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_user = new System.Windows.Forms.Label();
            this.label_password = new System.Windows.Forms.Label();
            this.label_password2 = new System.Windows.Forms.Label();
            this.button_sign_up = new System.Windows.Forms.Button();
            this.goster1_checkbox = new System.Windows.Forms.CheckBox();
            this.goster2_checkbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // username_textbox
            // 
            this.username_textbox.BackColor = System.Drawing.Color.LavenderBlush;
            this.username_textbox.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_textbox.Location = new System.Drawing.Point(284, 123);
            this.username_textbox.Margin = new System.Windows.Forms.Padding(2);
            this.username_textbox.Name = "username_textbox";
            this.username_textbox.Size = new System.Drawing.Size(138, 36);
            this.username_textbox.TabIndex = 0;
            this.username_textbox.TextChanged += new System.EventHandler(this.username_textbox_TextChanged);
            // 
            // password_textbox
            // 
            this.password_textbox.BackColor = System.Drawing.Color.LavenderBlush;
            this.password_textbox.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_textbox.Location = new System.Drawing.Point(284, 175);
            this.password_textbox.Margin = new System.Windows.Forms.Padding(2);
            this.password_textbox.Name = "password_textbox";
            this.password_textbox.Size = new System.Drawing.Size(138, 36);
            this.password_textbox.TabIndex = 1;
            this.password_textbox.TextChanged += new System.EventHandler(this.password_textbox_TextChanged);
            // 
            // password2_textbox
            // 
            this.password2_textbox.BackColor = System.Drawing.Color.LavenderBlush;
            this.password2_textbox.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password2_textbox.Location = new System.Drawing.Point(284, 227);
            this.password2_textbox.Margin = new System.Windows.Forms.Padding(2);
            this.password2_textbox.Name = "password2_textbox";
            this.password2_textbox.Size = new System.Drawing.Size(138, 36);
            this.password2_textbox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(285, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 43);
            this.label1.TabIndex = 3;
            this.label1.Text = "SIGN UP";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_user
            // 
            this.label_user.AutoSize = true;
            this.label_user.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_user.Location = new System.Drawing.Point(183, 126);
            this.label_user.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_user.Name = "label_user";
            this.label_user.Size = new System.Drawing.Size(91, 28);
            this.label_user.TabIndex = 4;
            this.label_user.Text = "Username";
            this.label_user.Click += new System.EventHandler(this.label2_Click);
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_password.Location = new System.Drawing.Point(186, 178);
            this.label_password.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(88, 28);
            this.label_password.TabIndex = 5;
            this.label_password.Text = "Password";
            this.label_password.Click += new System.EventHandler(this.label_password_Click);
            // 
            // label_password2
            // 
            this.label_password2.AutoSize = true;
            this.label_password2.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_password2.Location = new System.Drawing.Point(114, 230);
            this.label_password2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_password2.Name = "label_password2";
            this.label_password2.Size = new System.Drawing.Size(160, 28);
            this.label_password2.TabIndex = 6;
            this.label_password2.Text = "Confirm Password";
            // 
            // button_sign_up
            // 
            this.button_sign_up.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_sign_up.Location = new System.Drawing.Point(295, 289);
            this.button_sign_up.Margin = new System.Windows.Forms.Padding(2);
            this.button_sign_up.Name = "button_sign_up";
            this.button_sign_up.Size = new System.Drawing.Size(118, 37);
            this.button_sign_up.TabIndex = 7;
            this.button_sign_up.Text = "Sign-up";
            this.button_sign_up.UseVisualStyleBackColor = true;
            this.button_sign_up.Click += new System.EventHandler(this.button_sign_up_Click);
            // 
            // goster1_checkbox
            // 
            this.goster1_checkbox.AutoSize = true;
            this.goster1_checkbox.Font = new System.Drawing.Font("Segoe Print", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goster1_checkbox.Location = new System.Drawing.Point(447, 184);
            this.goster1_checkbox.Margin = new System.Windows.Forms.Padding(2);
            this.goster1_checkbox.Name = "goster1_checkbox";
            this.goster1_checkbox.Size = new System.Drawing.Size(129, 27);
            this.goster1_checkbox.TabIndex = 8;
            this.goster1_checkbox.Text = "Show Password";
            this.goster1_checkbox.UseVisualStyleBackColor = true;
            this.goster1_checkbox.CheckedChanged += new System.EventHandler(this.goster1_checkbox_CheckedChanged);
            // 
            // goster2_checkbox
            // 
            this.goster2_checkbox.AutoSize = true;
            this.goster2_checkbox.Font = new System.Drawing.Font("Segoe Print", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goster2_checkbox.Location = new System.Drawing.Point(447, 232);
            this.goster2_checkbox.Margin = new System.Windows.Forms.Padding(2);
            this.goster2_checkbox.Name = "goster2_checkbox";
            this.goster2_checkbox.Size = new System.Drawing.Size(129, 27);
            this.goster2_checkbox.TabIndex = 9;
            this.goster2_checkbox.Text = "Show Password";
            this.goster2_checkbox.UseVisualStyleBackColor = true;
            this.goster2_checkbox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // sign_up
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(710, 427);
            this.Controls.Add(this.goster2_checkbox);
            this.Controls.Add(this.goster1_checkbox);
            this.Controls.Add(this.button_sign_up);
            this.Controls.Add(this.label_password2);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_user);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password2_textbox);
            this.Controls.Add(this.password_textbox);
            this.Controls.Add(this.username_textbox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "sign_up";
            this.Text = "Sign Up";
            this.Load += new System.EventHandler(this.sign_up_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox username_textbox;
        private System.Windows.Forms.TextBox password_textbox;
        private System.Windows.Forms.TextBox password2_textbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_user;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.Label label_password2;
        private System.Windows.Forms.Button button_sign_up;
        private System.Windows.Forms.CheckBox goster1_checkbox;
        private System.Windows.Forms.CheckBox goster2_checkbox;
    }
}