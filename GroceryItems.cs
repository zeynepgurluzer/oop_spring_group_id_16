﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace oop_windowsCalcLogin
{
    public class GroceryItems
    {
        private string name;
        private double price;
        private string description;
        private Image pictureBox;

        public GroceryItems(string name,double price,string description)
        {
            this.name = name;
            this.price = price;
            this.description = description;
        }
        public Image PictureBox
        {
            get
            {
                return pictureBox;
            }
            set
            {
                pictureBox = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
    }
    
}
