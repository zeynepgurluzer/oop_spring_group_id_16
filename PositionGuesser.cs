﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_windowsCalcLogin
{
    public partial class PositionGuesser : Form
    {
        private PositionGuesserGame game;
        public PositionGuesser()
        {
            InitializeComponent();
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }
        private void PositionGuesser_Load(object sender, EventArgs e)
        {
            game = new PositionGuesserGame();
        
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_return_Click(object sender, EventArgs e)
        {
            Selection selection = new Selection();
            selection.Show();
            this.Hide();
        }
       
        private bool defeatMessage(object sender, EventArgs e)
        {
            if (game.LastButton() == true)
            {
                DialogResult resultDefeat = MessageBox.Show("I COULD NOT FIND THE LOCATION PLEASE START AGAIN:(", "DEFEAT");
                if (resultDefeat == DialogResult.OK)
                {
                    this.Controls.Clear();
                    this.InitializeComponent();
                    this.PositionGuesser_Load(sender, e);

                }
                return true;
            }
            return false;
        }
        private void btn_start_Click(object sender, EventArgs e)
        {
            game.GuessIt();
            lbl_position.Text = "[" + game.guessRow.ToString() + "," + game.guessCol.ToString() + "]";
            lbl_istrue.Text = "Is my guess true?";
        }

        private void btn_true_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("YEEEEEEEEEAH I KNEW IT!" + Environment.NewLine + "Would you like to play another game?","VICTORY",MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                Selection selection = new Selection();
                selection.Show();
                this.Hide();
            } 
            else if (result == DialogResult.Yes)
            {
                this.Controls.Clear();
                this.InitializeComponent();
                this.PositionGuesser_Load(sender,e);
            }

        }
        private void btn_north_Click(object sender, EventArgs e)
        {

            game.goNorth();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }

        }

        private void btn_south_Click(object sender, EventArgs e)
        {
            game.goSouth();
            if(!defeatMessage(sender, e))
            {
               btn_start_Click(sender, e);
            }
        }
        private void btn_west_Click(object sender, EventArgs e)
        {
            game.goWest();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }

        private void btn_east_Click(object sender, EventArgs e)
        {
            game.goEast();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }

        private void btn_nw_Click(object sender, EventArgs e)
        {        
            game.goNorthWest();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }

        private void btn_ne_Click(object sender, EventArgs e)
        {
            game.goNorthEast();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }

        private void btn_sw_Click(object sender, EventArgs e)
        {
            game.goSouthWest();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }

        private void btn_se_Click(object sender, EventArgs e)
        {
            game.goSouthEast();
            if (!defeatMessage(sender, e))
            {
                btn_start_Click(sender, e);
            }
        }
    }
}
