﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using oop_windowsCalcLogin;
namespace oop_spring_1
{
    public partial class WindowsCalc : Form
    {
        public WindowsCalc()
        {
            InitializeComponent();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            bool check = false;
            if (textBox_input_1.TextLength > 0 && textBox_input_2.TextLength > 0) { check = true; }
            if (check == true)
            {
                var isNumeric1 = int.TryParse(textBox_input_1.Text, out int sayi_1);
                var isNumeric2 = int.TryParse(textBox_input_2.Text, out int sayi_2);
                if (isNumeric1 == true && isNumeric2 == true)
                {
                    int sum;
                    sum = sayi_1 + sayi_2;
                    labelResult.Text = (sum).ToString();
                    if (sum > 0)
                        labelResult.BackColor = Color.LightGreen;
                    else
                        labelResult.BackColor = Color.OrangeRed;
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            bool check = false;
            if (textBox_input_1.TextLength > 0 && textBox_input_2.TextLength > 0) { check = true; }
            if (check == true)
            {
                var isNumeric1 = int.TryParse(textBox_input_1.Text, out int sayi_1);
                var isNumeric2 = int.TryParse(textBox_input_2.Text, out int sayi_2);
                if (isNumeric1 == true && isNumeric2 == true)
                {
                    int sub;
                    sub = sayi_1 - sayi_2;
                    labelResult.Text = (sub).ToString();
                    if (sub > 0)
                        labelResult.BackColor = Color.LightGreen;
                    else
                        labelResult.BackColor = Color.OrangeRed;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool check = false;
            if (textBox_input_1.TextLength > 0 && textBox_input_2.TextLength > 0) { check = true; }
            if (check == true)
            {
                var isNumeric1 = int.TryParse(textBox_input_1.Text, out int sayi_1);
                var isNumeric2 = int.TryParse(textBox_input_2.Text, out int sayi_2);
                if (isNumeric1 == true && isNumeric2 == true)
                {
                    int mult;
                    mult = sayi_1 * sayi_2;
                    labelResult.Text = (mult).ToString();
                    if (mult > 0)
                        labelResult.BackColor = Color.LightGreen;
                    else
                        labelResult.BackColor = Color.OrangeRed;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)//division
        {
            bool check = false;
            if (textBox_input_1.TextLength > 0 && textBox_input_2.TextLength > 0) { check = true; }
            if (check == true)
            {
                var isNumeric1 = float.TryParse(textBox_input_1.Text, out float sayi_1);
                var isNumeric2 = float.TryParse(textBox_input_2.Text, out float sayi_2);
                if (isNumeric1 == true && isNumeric2 == true)
                {
                    if (sayi_2 != 0) 
                    {
                        float div;
                        div = sayi_1 / sayi_2;
                        labelResult.Text = (div).ToString();
                        if (div > 0)
                            labelResult.BackColor = Color.LightGreen;
                        else
                            labelResult.BackColor = Color.OrangeRed;
                    }
                    else
                    {
                        labelResult.Text = "Error!";
                        labelResult.BackColor = Color.LightPink;
                    }
                }
            }

        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            button1.ForeColor = System.Drawing.Color.Red;
        }

        private void WindowsCalc_MouseLeave(object sender, EventArgs e)
        {
        }
        private void button2_MouseHover(object sender, EventArgs e)
        {
            button2.ForeColor = System.Drawing.Color.Red;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = System.Drawing.Color.Black;
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            button3.ForeColor = System.Drawing.Color.Red;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = System.Drawing.Color.Black;
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
            button4.ForeColor = System.Drawing.Color.Red;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.ForeColor = System.Drawing.Color.Black;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = System.Drawing.Color.Black;
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var control in this.Controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    Button but = new Button();
                    but = (Button)control;
                    if (but.BackColor != System.Windows.Forms.Button.DefaultBackColor)
                    {
                        but.BackColor = System.Windows.Forms.Button.DefaultBackColor;
                    }
                }
            }
            button1.BackColor = System.Drawing.Color.YellowGreen;
        }

        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var control in this.Controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    Button but = new Button();
                    but = (Button)control;
                    if (but.BackColor != System.Windows.Forms.Button.DefaultBackColor)
                    {
                        but.BackColor = System.Windows.Forms.Button.DefaultBackColor;
                    }
                }
            }
            button2.BackColor = System.Drawing.Color.YellowGreen;
    
        }

        private void button3_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var control in this.Controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    Button but = new Button();
                    but = (Button)control;
                    if (but.BackColor != System.Windows.Forms.Button.DefaultBackColor)
                    {
                        but.BackColor = System.Windows.Forms.Button.DefaultBackColor;
                    }
                }
            }
            button3.BackColor = System.Drawing.Color.YellowGreen;
        }

        private void button4_MouseClick(object sender, MouseEventArgs e)
        {
            foreach (var control in this.Controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    Button but = new Button();
                    but = (Button)control;
                    if (but.BackColor != System.Windows.Forms.Button.DefaultBackColor)
                    {
                        but.BackColor = System.Windows.Forms.Button.DefaultBackColor;
                    }
                }
            }
            button4.BackColor = System.Drawing.Color.YellowGreen;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Selection selection = new Selection();
            selection.Show();
            this.Hide();
        }
    }
}
