﻿
namespace oop_windowsCalcLogin
{
    partial class PositionGuesser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_north = new System.Windows.Forms.Button();
            this.btn_west = new System.Windows.Forms.Button();
            this.btn_east = new System.Windows.Forms.Button();
            this.btn_south = new System.Windows.Forms.Button();
            this.btn_ne = new System.Windows.Forms.Button();
            this.btn_se = new System.Windows.Forms.Button();
            this.btn_nw = new System.Windows.Forms.Button();
            this.btn_sw = new System.Windows.Forms.Button();
            this.pictureBox_pusula = new System.Windows.Forms.PictureBox();
            this.lbl_position = new System.Windows.Forms.Label();
            this.pictureBox_return = new System.Windows.Forms.PictureBox();
            this.lbl_guesstext = new System.Windows.Forms.Label();
            this.lbl_header = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.lbl_istrue = new System.Windows.Forms.Label();
            this.btn_true = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_pusula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_return)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_north
            // 
            this.btn_north.BackColor = System.Drawing.Color.CadetBlue;
            this.btn_north.FlatAppearance.BorderSize = 0;
            this.btn_north.Font = new System.Drawing.Font("Segoe Print", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_north.Location = new System.Drawing.Point(508, 148);
            this.btn_north.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_north.Name = "btn_north";
            this.btn_north.Size = new System.Drawing.Size(55, 55);
            this.btn_north.TabIndex = 1;
            this.btn_north.Text = "N";
            this.btn_north.UseVisualStyleBackColor = false;
            this.btn_north.Click += new System.EventHandler(this.btn_north_Click);
            // 
            // btn_west
            // 
            this.btn_west.BackColor = System.Drawing.Color.CadetBlue;
            this.btn_west.Font = new System.Drawing.Font("Segoe Print", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_west.Location = new System.Drawing.Point(432, 216);
            this.btn_west.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_west.Name = "btn_west";
            this.btn_west.Size = new System.Drawing.Size(55, 55);
            this.btn_west.TabIndex = 2;
            this.btn_west.Text = "W";
            this.btn_west.UseVisualStyleBackColor = false;
            this.btn_west.Click += new System.EventHandler(this.btn_west_Click);
            // 
            // btn_east
            // 
            this.btn_east.BackColor = System.Drawing.Color.CadetBlue;
            this.btn_east.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_east.FlatAppearance.BorderSize = 0;
            this.btn_east.Font = new System.Drawing.Font("Segoe Print", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_east.Location = new System.Drawing.Point(584, 216);
            this.btn_east.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_east.Name = "btn_east";
            this.btn_east.Size = new System.Drawing.Size(55, 55);
            this.btn_east.TabIndex = 3;
            this.btn_east.Text = "E";
            this.btn_east.UseVisualStyleBackColor = false;
            this.btn_east.Click += new System.EventHandler(this.btn_east_Click);
            // 
            // btn_south
            // 
            this.btn_south.BackColor = System.Drawing.Color.CadetBlue;
            this.btn_south.Font = new System.Drawing.Font("Segoe Print", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_south.Location = new System.Drawing.Point(508, 288);
            this.btn_south.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_south.Name = "btn_south";
            this.btn_south.Size = new System.Drawing.Size(55, 55);
            this.btn_south.TabIndex = 4;
            this.btn_south.Text = "S";
            this.btn_south.UseVisualStyleBackColor = false;
            this.btn_south.Click += new System.EventHandler(this.btn_south_Click);
            // 
            // btn_ne
            // 
            this.btn_ne.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_ne.Font = new System.Drawing.Font("Segoe Print", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_ne.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_ne.Location = new System.Drawing.Point(584, 156);
            this.btn_ne.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ne.Name = "btn_ne";
            this.btn_ne.Size = new System.Drawing.Size(45, 45);
            this.btn_ne.TabIndex = 5;
            this.btn_ne.Text = "NE";
            this.btn_ne.UseVisualStyleBackColor = false;
            this.btn_ne.Click += new System.EventHandler(this.btn_ne_Click);
            // 
            // btn_se
            // 
            this.btn_se.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_se.Font = new System.Drawing.Font("Segoe Print", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_se.Location = new System.Drawing.Point(584, 289);
            this.btn_se.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_se.Name = "btn_se";
            this.btn_se.Size = new System.Drawing.Size(45, 45);
            this.btn_se.TabIndex = 6;
            this.btn_se.Text = "SE";
            this.btn_se.UseVisualStyleBackColor = false;
            this.btn_se.Click += new System.EventHandler(this.btn_se_Click);
            // 
            // btn_nw
            // 
            this.btn_nw.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_nw.Font = new System.Drawing.Font("Segoe Print", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_nw.Location = new System.Drawing.Point(442, 156);
            this.btn_nw.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_nw.Name = "btn_nw";
            this.btn_nw.Size = new System.Drawing.Size(45, 45);
            this.btn_nw.TabIndex = 7;
            this.btn_nw.Text = "NW";
            this.btn_nw.UseVisualStyleBackColor = false;
            this.btn_nw.Click += new System.EventHandler(this.btn_nw_Click);
            // 
            // btn_sw
            // 
            this.btn_sw.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_sw.Font = new System.Drawing.Font("Segoe Print", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_sw.Location = new System.Drawing.Point(442, 289);
            this.btn_sw.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_sw.Name = "btn_sw";
            this.btn_sw.Size = new System.Drawing.Size(45, 45);
            this.btn_sw.TabIndex = 8;
            this.btn_sw.Text = "SW";
            this.btn_sw.UseVisualStyleBackColor = false;
            this.btn_sw.Click += new System.EventHandler(this.btn_sw_Click);
            // 
            // pictureBox_pusula
            // 
            this.pictureBox_pusula.Image = global::oop_windowsCalcLogin.Properties.Resources.pusula;
            this.pictureBox_pusula.Location = new System.Drawing.Point(503, 213);
            this.pictureBox_pusula.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox_pusula.Name = "pictureBox_pusula";
            this.pictureBox_pusula.Size = new System.Drawing.Size(65, 65);
            this.pictureBox_pusula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_pusula.TabIndex = 9;
            this.pictureBox_pusula.TabStop = false;
            this.pictureBox_pusula.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lbl_position
            // 
            this.lbl_position.AutoSize = true;
            this.lbl_position.Font = new System.Drawing.Font("Segoe Print", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_position.Location = new System.Drawing.Point(180, 198);
            this.lbl_position.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_position.Name = "lbl_position";
            this.lbl_position.Size = new System.Drawing.Size(0, 33);
            this.lbl_position.TabIndex = 10;
            // 
            // pictureBox_return
            // 
            this.pictureBox_return.Image = global::oop_windowsCalcLogin.Properties.Resources.free_curved_arrow_icon_2261_thumb;
            this.pictureBox_return.Location = new System.Drawing.Point(24, 39);
            this.pictureBox_return.Name = "pictureBox_return";
            this.pictureBox_return.Size = new System.Drawing.Size(80, 45);
            this.pictureBox_return.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_return.TabIndex = 11;
            this.pictureBox_return.TabStop = false;
            this.pictureBox_return.Click += new System.EventHandler(this.pictureBox_return_Click);
            // 
            // lbl_guesstext
            // 
            this.lbl_guesstext.AutoSize = true;
            this.lbl_guesstext.Font = new System.Drawing.Font("Segoe Print", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guesstext.Location = new System.Drawing.Point(102, 198);
            this.lbl_guesstext.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_guesstext.Name = "lbl_guesstext";
            this.lbl_guesstext.Size = new System.Drawing.Size(74, 33);
            this.lbl_guesstext.TabIndex = 13;
            this.lbl_guesstext.Text = "Guess:";
            // 
            // lbl_header
            // 
            this.lbl_header.AutoSize = true;
            this.lbl_header.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lbl_header.Location = new System.Drawing.Point(216, 41);
            this.lbl_header.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(267, 43);
            this.lbl_header.TabIndex = 14;
            this.lbl_header.Text = "POSITION GUESSER";
            // 
            // btn_start
            // 
            this.btn_start.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_start.Location = new System.Drawing.Point(108, 148);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(175, 37);
            this.btn_start.TabIndex = 15;
            this.btn_start.Text = "Start Game";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lbl_istrue
            // 
            this.lbl_istrue.AutoSize = true;
            this.lbl_istrue.Font = new System.Drawing.Font("Segoe Print", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_istrue.Location = new System.Drawing.Point(107, 273);
            this.lbl_istrue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_istrue.Name = "lbl_istrue";
            this.lbl_istrue.Size = new System.Drawing.Size(0, 33);
            this.lbl_istrue.TabIndex = 16;
            // 
            // btn_true
            // 
            this.btn_true.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_true.Location = new System.Drawing.Point(108, 316);
            this.btn_true.Name = "btn_true";
            this.btn_true.Size = new System.Drawing.Size(175, 37);
            this.btn_true.TabIndex = 17;
            this.btn_true.Text = "True";
            this.btn_true.UseVisualStyleBackColor = true;
            this.btn_true.Click += new System.EventHandler(this.btn_true_Click);
            // 
            // PositionGuesser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(710, 427);
            this.Controls.Add(this.btn_true);
            this.Controls.Add(this.lbl_istrue);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.lbl_header);
            this.Controls.Add(this.lbl_guesstext);
            this.Controls.Add(this.pictureBox_return);
            this.Controls.Add(this.lbl_position);
            this.Controls.Add(this.pictureBox_pusula);
            this.Controls.Add(this.btn_sw);
            this.Controls.Add(this.btn_nw);
            this.Controls.Add(this.btn_se);
            this.Controls.Add(this.btn_ne);
            this.Controls.Add(this.btn_south);
            this.Controls.Add(this.btn_east);
            this.Controls.Add(this.btn_west);
            this.Controls.Add(this.btn_north);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "PositionGuesser";
            this.Text = "PositionGuesser";
            this.Load += new System.EventHandler(this.PositionGuesser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_pusula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_return)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_north;
        private System.Windows.Forms.Button btn_west;
        private System.Windows.Forms.Button btn_east;
        private System.Windows.Forms.Button btn_south;
        private System.Windows.Forms.Button btn_ne;
        private System.Windows.Forms.Button btn_se;
        private System.Windows.Forms.Button btn_nw;
        private System.Windows.Forms.Button btn_sw;
        private System.Windows.Forms.PictureBox pictureBox_pusula;
        private System.Windows.Forms.Label lbl_position;
        private System.Windows.Forms.PictureBox pictureBox_return;
        private System.Windows.Forms.Label lbl_guesstext;
        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lbl_istrue;
        private System.Windows.Forms.Button btn_true;
    }
}