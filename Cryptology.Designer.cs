﻿
namespace oop_windowsCalcLogin
{
    partial class Cryptology
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCeaserE = new System.Windows.Forms.RadioButton();
            this.btnCeaserD = new System.Windows.Forms.RadioButton();
            this.btnVigenèreE = new System.Windows.Forms.RadioButton();
            this.btnVigenèreD = new System.Windows.Forms.RadioButton();
            this.lblResult = new System.Windows.Forms.Label();
            this.txtAlphabet = new System.Windows.Forms.TextBox();
            this.txtString = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblAlphabet = new System.Windows.Forms.Label();
            this.lblString = new System.Windows.Forms.Label();
            this.lblKey = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.PctBoxArrow = new System.Windows.Forms.PictureBox();
            this.lblRes2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PctBoxArrow)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCeaserE
            // 
            this.btnCeaserE.AutoSize = true;
            this.btnCeaserE.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCeaserE.Location = new System.Drawing.Point(399, 138);
            this.btnCeaserE.Margin = new System.Windows.Forms.Padding(2);
            this.btnCeaserE.Name = "btnCeaserE";
            this.btnCeaserE.Size = new System.Drawing.Size(234, 32);
            this.btnCeaserE.TabIndex = 0;
            this.btnCeaserE.TabStop = true;
            this.btnCeaserE.Text = "Ceaser Cipher Encryption";
            this.btnCeaserE.UseVisualStyleBackColor = true;
            this.btnCeaserE.CheckedChanged += new System.EventHandler(this.btnCeaserE_CheckedChanged);
            // 
            // btnCeaserD
            // 
            this.btnCeaserD.AutoSize = true;
            this.btnCeaserD.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCeaserD.Location = new System.Drawing.Point(399, 169);
            this.btnCeaserD.Margin = new System.Windows.Forms.Padding(2);
            this.btnCeaserD.Name = "btnCeaserD";
            this.btnCeaserD.Size = new System.Drawing.Size(233, 32);
            this.btnCeaserD.TabIndex = 1;
            this.btnCeaserD.TabStop = true;
            this.btnCeaserD.Text = "Ceaser Cipher Decryption";
            this.btnCeaserD.UseVisualStyleBackColor = true;
            this.btnCeaserD.CheckedChanged += new System.EventHandler(this.btnCeaserD_CheckedChanged);
            // 
            // btnVigenèreE
            // 
            this.btnVigenèreE.AutoSize = true;
            this.btnVigenèreE.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVigenèreE.Location = new System.Drawing.Point(399, 205);
            this.btnVigenèreE.Margin = new System.Windows.Forms.Padding(2);
            this.btnVigenèreE.Name = "btnVigenèreE";
            this.btnVigenèreE.Size = new System.Drawing.Size(251, 32);
            this.btnVigenèreE.TabIndex = 2;
            this.btnVigenèreE.TabStop = true;
            this.btnVigenèreE.Text = "Vigenère Cipher Encryption";
            this.btnVigenèreE.UseVisualStyleBackColor = true;
            this.btnVigenèreE.CheckedChanged += new System.EventHandler(this.btnVigenèreE_CheckedChanged);
            // 
            // btnVigenèreD
            // 
            this.btnVigenèreD.AutoSize = true;
            this.btnVigenèreD.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVigenèreD.Location = new System.Drawing.Point(399, 241);
            this.btnVigenèreD.Margin = new System.Windows.Forms.Padding(2);
            this.btnVigenèreD.Name = "btnVigenèreD";
            this.btnVigenèreD.Size = new System.Drawing.Size(250, 32);
            this.btnVigenèreD.TabIndex = 3;
            this.btnVigenèreD.TabStop = true;
            this.btnVigenèreD.Text = "Vigenère Cipher Decryption";
            this.btnVigenèreD.UseVisualStyleBackColor = true;
            this.btnVigenèreD.CheckedChanged += new System.EventHandler(this.btnVigenèreD_CheckedChanged);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblResult.Font = new System.Drawing.Font("Segoe Print", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(297, 316);
            this.lblResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 47);
            this.lblResult.TabIndex = 4;
            // 
            // txtAlphabet
            // 
            this.txtAlphabet.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtAlphabet.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAlphabet.Location = new System.Drawing.Point(191, 139);
            this.txtAlphabet.Margin = new System.Windows.Forms.Padding(2);
            this.txtAlphabet.Name = "txtAlphabet";
            this.txtAlphabet.Size = new System.Drawing.Size(169, 36);
            this.txtAlphabet.TabIndex = 5;
            this.txtAlphabet.TextChanged += new System.EventHandler(this.txtAlphabet_TextChanged);
            // 
            // txtString
            // 
            this.txtString.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtString.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtString.Location = new System.Drawing.Point(191, 184);
            this.txtString.Margin = new System.Windows.Forms.Padding(2);
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(169, 36);
            this.txtString.TabIndex = 6;
            this.txtString.TextChanged += new System.EventHandler(this.txtString_TextChanged);
            // 
            // txtKey
            // 
            this.txtKey.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtKey.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.Location = new System.Drawing.Point(191, 229);
            this.txtKey.Margin = new System.Windows.Forms.Padding(2);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(169, 36);
            this.txtKey.TabIndex = 7;
            this.txtKey.TextChanged += new System.EventHandler(this.txtKey_TextChanged);
            // 
            // lblAlphabet
            // 
            this.lblAlphabet.AutoSize = true;
            this.lblAlphabet.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlphabet.Location = new System.Drawing.Point(33, 142);
            this.lblAlphabet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAlphabet.Name = "lblAlphabet";
            this.lblAlphabet.Size = new System.Drawing.Size(133, 28);
            this.lblAlphabet.TabIndex = 8;
            this.lblAlphabet.Text = "Enter Alphabet";
            // 
            // lblString
            // 
            this.lblString.AutoSize = true;
            this.lblString.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblString.Location = new System.Drawing.Point(53, 187);
            this.lblString.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblString.Name = "lblString";
            this.lblString.Size = new System.Drawing.Size(113, 28);
            this.lblString.TabIndex = 9;
            this.lblString.Text = "Enter String";
            // 
            // lblKey
            // 
            this.lblKey.AutoSize = true;
            this.lblKey.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKey.Location = new System.Drawing.Point(74, 232);
            this.lblKey.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(92, 28);
            this.lblKey.TabIndex = 10;
            this.lblKey.Text = "Enter Key";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeading.Location = new System.Drawing.Point(256, 34);
            this.lblHeading.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(190, 43);
            this.lblHeading.TabIndex = 13;
            this.lblHeading.Text = "CRYPTOLOGY";
            this.lblHeading.Click += new System.EventHandler(this.lblHeading_Click);
            // 
            // PctBoxArrow
            // 
            this.PctBoxArrow.Image = global::oop_windowsCalcLogin.Properties.Resources.free_curved_arrow_icon_2261_thumb;
            this.PctBoxArrow.Location = new System.Drawing.Point(24, 39);
            this.PctBoxArrow.Margin = new System.Windows.Forms.Padding(2);
            this.PctBoxArrow.Name = "PctBoxArrow";
            this.PctBoxArrow.Size = new System.Drawing.Size(80, 45);
            this.PctBoxArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctBoxArrow.TabIndex = 12;
            this.PctBoxArrow.TabStop = false;
            this.PctBoxArrow.Click += new System.EventHandler(this.PctBoxArrow_Click);
            // 
            // lblRes2
            // 
            this.lblRes2.AutoSize = true;
            this.lblRes2.Font = new System.Drawing.Font("Segoe Print", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRes2.Location = new System.Drawing.Point(183, 316);
            this.lblRes2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRes2.Name = "lblRes2";
            this.lblRes2.Size = new System.Drawing.Size(98, 43);
            this.lblRes2.TabIndex = 14;
            this.lblRes2.Text = "Result:";
            // 
            // Cryptology
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(710, 427);
            this.Controls.Add(this.lblRes2);
            this.Controls.Add(this.lblHeading);
            this.Controls.Add(this.PctBoxArrow);
            this.Controls.Add(this.lblKey);
            this.Controls.Add(this.lblString);
            this.Controls.Add(this.lblAlphabet);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.txtString);
            this.Controls.Add(this.txtAlphabet);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.btnVigenèreD);
            this.Controls.Add(this.btnVigenèreE);
            this.Controls.Add(this.btnCeaserD);
            this.Controls.Add(this.btnCeaserE);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Cryptology";
            this.Text = "Cryptology";
            this.Load += new System.EventHandler(this.Cryptology_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PctBoxArrow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton btnCeaserE;
        private System.Windows.Forms.RadioButton btnCeaserD;
        private System.Windows.Forms.RadioButton btnVigenèreE;
        private System.Windows.Forms.RadioButton btnVigenèreD;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox txtAlphabet;
        private System.Windows.Forms.TextBox txtString;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblAlphabet;
        private System.Windows.Forms.Label lblString;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.PictureBox PctBoxArrow;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblRes2;
    }
}