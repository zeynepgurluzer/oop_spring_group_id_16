﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using oop_spring_1;

namespace oop_windowsCalcLogin
{
    
    public partial class loginForm : Form
    {
        internal User u;
       
        public loginForm()
        {
            InitializeComponent();
        }

        private void btngiris_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(btnusername.Text) && !String.IsNullOrEmpty(btnpassword.Text))
            {
                if (u.isValid(btnusername.Text, btnpassword.Text) == true)
                {
                    messageLabel.ForeColor = Color.Green;
                    messageLabel.Text = "Log in successful!";
                    Selection S = new Selection();
                    S.Show();
                    this.Hide();
                }
                else
                {
                    messageLabel.ForeColor = Color.Red;
                    messageLabel.Text = "Wrong username or password!";
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            u = new User();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            sign_up su = new sign_up(this);
            su.Show();
        }

        private void btnusername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
