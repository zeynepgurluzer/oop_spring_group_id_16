﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using oop_spring_1;

namespace oop_windowsCalcLogin
{
    public partial class Selection : Form
    {
        public Selection()
        {
            InitializeComponent();
        }
         protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }
        private void btnCalculator_Click(object sender, EventArgs e)
        {
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            loginForm Login = new loginForm();
            Login.Show();
            this.Hide();
        }

        private void btnCalculator_Click_1(object sender, EventArgs e)
        {
            WindowsCalc Calc = new WindowsCalc();
            Calc.Show();
            this.Hide();
        }

        private void btnencrypt_Click(object sender, EventArgs e)
        {
            Cryptology Cry = new Cryptology();
            Cry.Show();
            this.Hide();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            WindowsCalc Calc = new WindowsCalc();
            Calc.Show();
            this.Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Cryptology Cry = new Cryptology();
            Cry.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            loginForm Login = new loginForm();
            Login.Show();
            this.Hide();
        }

        private void Selection_Load(object sender, EventArgs e)
        {
            Rectangle r = new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height);
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            int d = 50;
            gp.AddArc(r.X, r.Y, d, d, 180, 90);
            gp.AddArc(r.X + r.Width - d, r.Y, d, d, 270, 90);
            gp.AddArc(r.X + r.Width - d, r.Y + r.Height - d, d, d, 0, 90);
            gp.AddArc(r.X, r.Y + r.Height - d, d, d, 90, 90);
            pictureBox1.Region = new Region(gp);

            Rectangle r1 = new Rectangle(0, 0, pictureBox3.Width, pictureBox3.Height);
            System.Drawing.Drawing2D.GraphicsPath gp1 = new System.Drawing.Drawing2D.GraphicsPath();

            gp1.AddArc(r1.X, r1.Y, d, d, 180, 90);
            gp1.AddArc(r1.X + r1.Width - d, r1.Y, d, d, 270, 90);
            gp1.AddArc(r1.X + r1.Width - d, r1.Y + r1.Height - d, d, d, 0, 90);
            gp1.AddArc(r1.X, r1.Y + r1.Height - d, d, d, 90, 90);
            pictureBox3.Region = new Region(gp1);

            Rectangle r2 = new Rectangle(0, 0, pictureBox_positionG.Width, pictureBox_positionG.Height);
            System.Drawing.Drawing2D.GraphicsPath gp2 = new System.Drawing.Drawing2D.GraphicsPath();
            gp2.AddArc(r2.X, r2.Y, d, d, 180, 90);
            gp2.AddArc(r2.X + r2.Width - d, r2.Y, d, d, 270, 90);
            gp2.AddArc(r2.X + r2.Width - d, r2.Y + r2.Height - d, d, d, 0, 90);
            gp2.AddArc(r2.X, r2.Y + r2.Height - d, d, d, 90, 90);
            pictureBox_positionG.Region = new Region(gp2);

            Rectangle r3 = new Rectangle(0, 0, pictureBox4.Width, pictureBox4.Height);
            System.Drawing.Drawing2D.GraphicsPath gp3 = new System.Drawing.Drawing2D.GraphicsPath();
            gp3.AddArc(r3.X, r3.Y, d, d, 180, 90);
            gp3.AddArc(r3.X + r3.Width - d, r3.Y, d, d, 270, 90);
            gp3.AddArc(r3.X + r3.Width - d, r3.Y + r3.Height - d, d, d, 0, 90);
            gp3.AddArc(r3.X, r3.Y + r3.Height - d, d, d, 90, 90);
            pictureBox4.Region = new Region(gp3);


        }

        private void pictureBox_positionG_Click(object sender, EventArgs e)
        {
            PositionGuesser pg = new PositionGuesser();
            pg.Show();
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            GroceryStoreAdminPanel GSAP = new GroceryStoreAdminPanel();
            GSAP.Show();
            this.Hide();
        }
    }
}
