﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace oop_windowsCalcLogin
{
    public class PositionGuesserGame
    {
        private static int mapSize = 1024;
        private int[,] map = new int[mapSize, mapSize];
        private int Guess;
        private int Row;
        private int Col;
        private int minRow = 0, maxRow = mapSize;
        private int minCol = 0, maxCol = mapSize;
        public int guessRow
        {
            get
            {
                return Row;
            }
            set
            {
                Row = value;
            }
        }
        public int guessCol
        {
            get
            {
                return Col;
            }
            set
            {
                Col = value;
            }
        }
        public int guess
        {
            get
            {
                return Guess;
            }
            set
            {
                Guess = value;
            }
        }
        public PositionGuesserGame()
        {
            int counter = 1;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    map[i, j] = counter;
                    counter++;
                }
            }
        }
        public bool LastButton()
        {
            if (minRow == maxRow && minCol == maxCol)
            {
                return true;
            }
            return false;
        }
        
        public void GuessIt()
        {
            RandomPosition();
        }
        public void RandomPosition()
        {
            Random randomNumber = new Random();
            Row = randomNumber.Next(minRow, maxRow);
            Col = randomNumber.Next(minCol, maxCol);
            guess = map[Row, Col];
        }   
        public void goNorth()
        {
            minRow = 0;
            maxRow = Row;
        }
        public void goSouth()
        {
            minRow = Row;
            maxRow = mapSize;
        }
        public void goEast()
        {
            minCol = Col;
            maxCol = mapSize;
        }
        public void goWest()
        {
            minCol = 0;
            maxCol = Col;
        }
        public void goNorthWest()
        {
            goWest();
            goNorth();
        }
        public void goNorthEast()
        {
            goEast();
            goNorth();
        }
        public void goSouthWest()
        {
            goSouth();
            goWest();
        }
        public void goSouthEast()
        {
            goSouth();
            goEast();
        }

    }
}
